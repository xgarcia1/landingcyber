import React, { useState } from 'react'
import axios from 'axios'
import { useForm } from 'react-hook-form'
import { useCssHandles } from 'vtex.css-handles'
// import { Helmet } from 'vtex.render-runtime'
// import ReactMarkdown from "react-markdown"

require('./css/style.css');
const CSS_HANDLES = [
    'container',
    'spacer',
    'form__container',
    "form__title",
    "form",
    "form__input_group",
    "form__input_group_textarea",
    "form__field",
    "form__button_container",
    "button_submit",
    "form__input_groupTC",
    "form__flex",
    "form__input_groupTC_container",
    "campoRequerido",
    "lr_h3",
    "banner_initial_cyber",
    "banner_initial_cyber_mobile",
    "banner_medium_cyber",
    "preguntas",
    "container-fluid",
    "titulo",
    "text-center",
    "container",
    "pre-rest_description",
    "cyber-h1",
    "pre_respt",
    "tit",
    "numero",
    "respt",
    "icon",
    "txtLegales",
    "messageText",
    "CheckContainerGeneral",
    "CheckContainer"
]


function Cyber() {
    console.log('llamar funcion')
    // const [checked, setChecked] = React.useState(true);
    const handles = useCssHandles(CSS_HANDLES);
    const [checkbox, setCheck] = useState(false);
    const [checkbox1, setCheck1] = useState(false);

    const [message, setMessage] = useState("");
    // const [succesRegister, setSuccesRegister] = useState(false);
    // const [lodaing, setLoading] = useState(false);

    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();

    const onSubmit = async (data: any, e: any) => {
        console.log(data)

        if (checkbox1 === false) {
            return
        } else {


            registerEmail(data.email, data, e, checkbox)

            // axios({
            //     method: 'POST',
            //     url: `/api/dataentities/FS/documents/`,
            //     data: {
            //         // direccion: data.direccion,
            //         // distrito: data.distrito,
            //         // documentoIdentidad: data.documentoIdentidad,
            //         // email: data.email,
            //         nombre: data.nombre,
            //         apellido: data.apellido,
            //         celular: data.celular,
            //         email: data.email,
            //         shopping: true
            //         // nrodocumento: data.nrodocumento,
            //         // nropedidos: data.nropedidos,
            //         // provincia: data.provincia,
            //         // region: data.region,
            //         // telefono: data.telefono,
            //         // terminosycondiciones: data.terminosycondiciones,
            //     }
            // }).then((result) => {
            //     console.log("result==>", result);

            //     /* return result; */
            // }).then(() => {
            //     setMessage("Su correo se ha registrado exitosamente");
            //     e.target.reset();

            // }).catch((error) => console.log("error==>", error));


        }
    }

    const registerEmail = async (email: any, data: any, e: any, checkbox: boolean) => {
        // setLoading(true);
        let res = await axios({
            method: "get",
            url: `/api/dataentities/FS/search?_where=email=${email}&_fields=shopping`,
        });

        console.log(res.data)

        if (res.data.length > 0) {

            if (res.data[0].shopping) {
                // setSuccesRegister(false);
                console.log('repite correo')
                // setLoading(false);
                setMessage(`El correo ${email} ya se encuentra registrado.`);
                return;
            }
        } else {
            console.log('correo nuevo')
            await axios({
                method: 'POST',
                url: `/api/dataentities/FS/documents/`,
                data: {
                    // direccion: data.direccion,
                    // distrito: data.distrito,
                    // documentoIdentidad: data.documentoIdentidad,
                    // email: data.email,
                    nombre: data.nombre,
                    apellido: data.apellido,
                    celular: data.celular,
                    email: data.email,
                    shopping: true,
                    promocion: checkbox

                }
            }).then((result) => {
                console.log("result==>", result);

                /* return result; */
            }).then(() => {
                // setSuccesRegister(true);
                setMessage("Su correo se ha registrado exitosamente");
                e.target.reset();
            }).catch((error) => console.log("error==>", error));
        }
        // setLoading(false);
    };

    return (

          <div className={handles.container}>
        
            <div className={handles.spacer}></div>
            <div>
                <img className={handles.banner_initial_cyber} src="https://clubsoftyspe.vtexassets.com/arquivos/02BannerInicio_Dsktp-04-10-2021.jpg" alt="Ofertas de Cyber Wow 2021" />
                <img className={handles.banner_initial_cyber_mobile} src="https://clubsoftyspe.vtexassets.com/arquivos/02BannerInicio_Mobile-04-10-2021.jpg" alt="Ofertas de Cyber Wow 2021" />
            </div>
            <div className={handles.form__container}>

                <h3 className={handles.form__title}>Suscríbete </h3>

                <div className={handles.form__flex}>
                    <div className={handles.banner_medium_cyber}>
                        <img src="https://clubsoftyspe.vtexassets.com/arquivos/01BannerMedio_DktpCYBER.jpg" alt="" />
                        <p>¡Llega el evento más esperado de la internet! Regístrate y recibe las mejores promociones del Cyber Wow en Club Softys. </p>
                    </div>
                    <form onSubmit={handleSubmit(onSubmit)} className={handles.form}>
                        <div >
                            <p className={handles.messageText}>{message}</p>
                        </div>

                        <h3>Datos personales</h3>
                        <div className={handles.form__input_group}>
                            <label htmlFor="nombre">1.	Nombres *:</label>
                            <input className={handles.form__field} placeholder="Ingrese su nombre completo"
                                {...register("nombre", {
                                    required: "Campo requerido",
                                })}
                            />
                            {errors.nombre && <span>Campo requerído</span>}
                        </div>

                        <div className={handles.form__input_group}>
                            <label htmlFor="apellido">2.	Apellidos  *:</label>
                            <input className={handles.form__field} placeholder="Ingrese sus apellidos"
                                {...register("apellido", {
                                    required: "Campo requerido",
                                })}
                            />
                            {errors.apellido && <span>Campo requerído</span>}
                        </div>

                        <div className={handles.form__input_group}>
                            <label htmlFor="email">3.	Email *:</label>
                            <input className={handles.form__field} placeholder="Ingrese su correo electrónico"
                                {...register("email", {
                                    required: "Campo requerido",
                                    pattern: {
                                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                        message: "Ingresa un Email válido",
                                    },
                                })}
                            />
                            {errors.email && <span>Campo requerído</span>}
                        </div>

                        <div className={handles.form__input_group}>
                            <label htmlFor="celular">4. Celular *:</label>
                            <input className={handles.form__field} placeholder="Ingrese su número de celular"
                                {...register("celular", {
                                    required: "Campo requerido",
                                })}
                            />
                            {errors.celular && <span>Campo requerído</span>}
                        </div>
                        {/* <div className={handles.form__input_group}>
                        <label htmlFor="documentoIdentidad">2. Documentos de Identidad</label>
                        <select className={handles.form__field}
                            {...register("documentoIdentidad", {
                                required: "Campo requerido",
                            })}
                        >
                            <option value="DNI">DNI</option>
                            <option value="CE">CE</option>
                        </select>
                        {errors.documentoIdentidad && <span>Campo requerído</span>}
                    </div>
                    <div className={handles.form__input_group}>
                        <label htmlFor="nrodocumento">3. Numero de documento *:</label>
                        <input className={handles.form__field} placeholder="99999999"
                            {...register("nrodocumento", {
                                required: "Campo requerido",
                            })}
                        />
                        {errors.nrodocumento && <span>Campo requerído</span>}
                    </div> */}



                        {/* <h3>Datos de contacto</h3>
                    <div className={handles.form__input_group}>
                        <label htmlFor="email">4. E-mail *:</label>
                        <input placeholder="Ingresa tu mail" className={handles.form__field}
                            {...register("email", {
                                required: "Campo requerido",
                                pattern: {
                                    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                    message: "Ingresa un Email válido",
                                },
                            })} />
                        {errors.email && <span> Ingresa un Email válido</span>}
                    </div>
                    <div className={handles.form__input_group}>
                        <label htmlFor="telefono">5. Teléfono móvil*:</label>
                        <input placeholder="989375465" className={handles.form__field}
                            {...register("telefono", {
                                required: "Campo requerido",
                            })}
                        />
                        {errors.telefono && <span> Campo requerído</span>}
                    </div>
                    <div className={handles.form__input_group}>
                        <label htmlFor="direccion">6. Dirección *:</label>
                        <input placeholder="Ingrese su dirección" className={handles.form__field}
                            {...register("direccion", {
                                required: "Campo requerido",
                            })}
                        />
                        {errors.direccion && <span>Campo requerído</span>}
                    </div>

                    <div className={handles.form__input_group}>
                        <label htmlFor="distrito">7. Distrito *:</label>
                        <input placeholder="Ingrese su distrito" className={handles.form__field}
                            {...register("distrito", {
                                required: "Campo requerido",
                            })}
                        />
                        {errors.distrito && <span>Campo requerído</span>}
                    </div>


                    <div className={handles.form__input_group}>
                        <label htmlFor="provincia">8. Provincia *:</label>
                        <input placeholder="Ingrese su provincia" className={handles.form__field}
                            {...register("provincia", {
                                required: "Campo requerido",
                            })}
                        />
                        {errors.provincia && <span>Campo requerído</span>}
                    </div>

                    <div className={handles.form__input_group}>
                        <label htmlFor="region">9. Región *:</label>
                        <input placeholder="Ingrese su Región" className={handles.form__field}
                            {...register("region", {
                                required: "Campo requerido",
                            })}
                        />
                        {errors.region && <span>Campo requerído</span>}
                    </div> */}

                        {/* 
                    <h3>Detalles de la compra</h3>

                    <div className={handles.form__input_group}>
                        <label htmlFor="nropedidos">10. Número de pedido *:</label>
                        <input className={handles.form__field}
                            {...register("nropedidos", {
                                required: "Campo requerido",
                            })}
                        />
                        {errors.nropedidos && <span>Campo requerído</span>}
                    </div>

                    <div className={handles.form__input_group}> */}
                        {/* <h3>Los pasos para conocer el número de pedido son:</h3>
                        <p>1.- Hacer clic en "Mi cuenta"</p>
                        <p>2.- Luego en la sección "Pedidos" que está en el lado izquierdo.</p>
                        <img src="https://eliteprofessionalpe.vtexassets.com/arquivos/image-pedidos-23-08-2021.png" alt="" />
                    </div> */}

                        {/* <div className={handles.form__input_groupTC_container}>
                        <div className={handles.form__input_groupTC}>
                            <label htmlFor="terminosycondiciones">Declaro haber leído y aceptado las  <a className={handles.checkTerminosyCondiciones} target="_blank" href="https://www.eliteprofessional.com.pe/ayuda/terminos-condiciones">políticas de privacidad</a></label>
                            <input type="checkbox"
                                defaultChecked={checked}
                                onChange={() => setChecked(!checked)} */}
                        {/* /> */}
                        {/* <input type="checkbox" className={handles.form__field}
                                {...register("terminosycondiciones", {
                                    required: "Campo requerido",
                                })}
                            /> */}

                        {/* <span style={{ color: "red" }}>Acepta términos y condiciones para continuar</span> */}
                        {/* </div> */}

                        {/* <div className={handles.form__input_groupTC}>
                            <label htmlFor="terminosycondiciones">Declaro haber leído y aceptado los   <a className={handles.checkTerminosyCondiciones} target="_blank" href="https://eliteprofessionalpe.vteximg.com.br/arquivos/BasesConcursosyPromociones-CanastaAniversario(VF).pdf"> términos y condiciones del concurso.</a></label>
                            <input type="checkbox"
                                defaultChecked={checked}
                                onChange={() => setChecked(!checked)}
                            /> */}

                        {/* <span style={{ color: "red" }} >Acepta términos y condiciones para continuar</span> */}
                        {/* </div> */}
                        <div className={handles.CheckContainerGeneral}>
                            <div className={`${handles.CheckContainer} flex items-start`}>
                                <input
                                    type="checkbox"
                                    checked={checkbox1}
                                    onChange={() => setCheck1(!checkbox1)}
                                    id="acceptTerms"

                                />
                                <label htmlFor="acceptTerms" className="mh1"><a href="https://www.clubsoftys.com.pe/terminos-condiciones">Acepto términos y condiciones     </a>    y   <a href="https://www.clubsoftys.com.pe/politicas-privacidad">  Acepto las Políticas de Privacidad</a>

                                </label>
                            </div>

                            {/* <div className={`${handles.CheckContainer} flex items-start`}>
                                <input
                                    type="checkbox"
                                    checked={checkbox2}
                                    onChange={() => setCheck2(!checkbox2)}
                                    id="acceptTerms"

                                />
                                <label htmlFor="acceptTerms" className="mh1"><a href="https://www.clubsoftys.com.pe/politicas-privacidad">Acepto las Políticas de Privacidad</a>
                                </label>
                            </div> */}
                            <div className={`${handles.CheckContainer} flex items-start`}>
                                <input
                                    type="checkbox"
                                    checked={checkbox}
                                    onChange={() => setCheck(!checkbox)}
                                    id="acceptTerms"

                                />
                                <label htmlFor="acceptTerms" className="mh1">Deseo recibir promociones a mi correo
                                </label>
                            </div>
                        </div>
                        {/* <div className={`${handles.message}`}>{messageTerminos}</div> */}

                        <div className={handles.form__button_container}>
                            <button type="submit" className={handles.button_submit}>Enviar</button>
                        </div>
                        {/* </div> */}

                    </form>
                </div>


            </div>
            <div>
                <section className={handles.preguntas}>
                    <div className="container-fluid">
                        <div className="titulo text-center">
                            <h1>Cyber Wow 2021</h1>
                            <h2>Preguntas Frecuentes</h2>
                        </div>
                    </div>
                    <div className="container">
                        <p className="pre-rest_description">Responde todas tus preguntas frecuentes sobre</p>
                        <h3 className="cyber-h1">Cyber Clubsoftys</h3>
                        <div className="pre_respt">
                            <div className={handles.tit} >
                                <h2>
                                    <span className="numero"></span>
                                    ¿Qué es Cyber Wow en Perú?
                                    <span className="icon"></span>
                                </h2>
                            </div>
                            <div className={handles.respt}  >
                                <p>
                                    El Cyber Wow es una de las campañas digitales de comercio electrónico más grande del Perú, que junta a importantes marcas y empresas para que puedan ofertar sus productos con los mejores descuentos. La organización del evento está a cargo del Interactive Advertising Bureau (IAB).
                                </p>
                            </div>
                            <div className={handles.tit}>
                                <h2>
                                    <span className="numero"></span>
                                    ¿Cuándo es el Cyber Wow 2021 en Perú?
                                </h2>
                            </div>
                            <div className="respt" >
                                <p>
                                    El Cyber Wow se desarrolla 3 veces al año, durante los meses de abril, julio y octubre. Las fechas exactas del evento se comunican dos semanas antes de que empiece.
                                </p>
                            </div>
                            <div className={handles.tit}>
                                <h3>
                                    <span className="numero"></span>
                                    ¿Es segura la compra online durante Cyber?
                                </h3>
                            </div>
                            <div className="respt" >
                                <p>
                                    Club Softys cuenta con todos certificados de seguridad y protección al consumidor. Es importante destacar que el sitio web ha pasado por las revisiones correspondientes de Ethical Hacking y seguridad en todo lo relacionado a e-commerce. Compra en Club Softys es 100% seguro.
                                </p>
                            </div>
                            <div className={handles.tit}>
                                <h3>
                                    <span className="numero"></span>
                                    ¿Qué métodos de pago existen en Cyber?
                                </h3>
                            </div>
                            <div className="respt" >
                                <p>
                                    En Club Softys puedes pagar durante el evento con tus tarjetas Visa, Mastercard, American Express y Diners Club.
                                </p>
                            </div>
                            <div className={handles.tit}>
                                <h3>
                                    <span className="numero"></span>
                                    ¿Qué comprar durante el evento de Cyber?
                                </h3>
                            </div>
                            <div className="respt">
                                <p>
                                    Aprovecha el evento del Cyber para que puedas comprar los pañales para tu bebé, el papel higiénico para tu hogar o las sabanillas para tu mascota. En el Cyber encontrarás grandes descuentos para que puedas abastecer tu hogar al mejor precio.                                </p>
                            </div>
                            <div className={handles.tit}>
                                <h3>
                                    <span className="numero"></span>
                                    ¿Cómo comprar en el Cyber Wow?
                                </h3>
                            </div>
                            <div className="respt">
                                <p>
                                    Para comprar durante el Cyber, puedes hacerlo desde <a href="https://www.clubsoftys.com.pe/">ClubSoftys.com.pe</a>
                                </p>
                            </div>
                            <div className={handles.tit}>
                                <h3>
                                    <span className="numero"></span>
                                    ¿Cuándo llegarán mis productos comprados?
                                </h3>
                            </div>
                            <div className="respt">
                                <p>
                                    Se te enviará una correo de confirmación luego de haber pagado tu pedido con la fecha en que estará llegando el producto a la puerta de tu casa. Nuestros pedidos promesa son de hasta 48 horas.
                                </p>
                            </div>
                            {/* <div className={handles.tit}>
                                <h3>
                                    <span className="numero">8.</span>
                                    ¿Hay restricciones de pago y envío?
                                </h3>
                            </div> */}
                            {/* <div className="respt">
                                <p>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad maxime totam fuga, officia ducimus dolore maiores repellendus voluptatem cumque corrupti earum atque laboriosam aut, quos neque. Recusandae quasi consectetur numquam?
                                </p>
                            </div> */}
                        </div>


                    </div>

                </section>
            
            {/* <hr />
            <div><h2>LEGAL DE CAMPAÑA</h2></div>
            <p className={handles.txtLegales}>
                El sortero se realizará el 25 de octubre a las 10:00 am y se le enviará al ganador por el correo registrado el cupón ganador el 25 de octubre a las 11:00 am. El ganador será anunciado en los términos y condiciones de la campaña. Cupón no acumulable. Cupón no cubre el costo del envío. Cupón válido para los días 25 al 29 de octubre. Cupón por descuento máximo de S/100.
            </p> */}

        </div >
        </div>
    )
}

export default Cyber
